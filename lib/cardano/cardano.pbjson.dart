///
//  Generated code. Do not modify.
//  source: cardano/cardano.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use saveStatisticRequestDescriptor instead')
const SaveStatisticRequest$json = const {
  '1': 'SaveStatisticRequest',
  '2': const [
    const {'1': 'node_auth_data', '3': 1, '4': 1, '5': 11, '6': '.Common.NodeAuthData', '10': 'nodeAuthData'},
    const {'1': 'statistic', '3': 2, '4': 1, '5': 11, '6': '.cardano.StatisticRequest', '10': 'statistic'},
  ],
};

/// Descriptor for `SaveStatisticRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List saveStatisticRequestDescriptor = $convert.base64Decode('ChRTYXZlU3RhdGlzdGljUmVxdWVzdBI6Cg5ub2RlX2F1dGhfZGF0YRgBIAEoCzIULkNvbW1vbi5Ob2RlQXV0aERhdGFSDG5vZGVBdXRoRGF0YRI3CglzdGF0aXN0aWMYAiABKAsyGS5jYXJkYW5vLlN0YXRpc3RpY1JlcXVlc3RSCXN0YXRpc3RpYw==');
@$core.Deprecated('Use saveStatisticResponseDescriptor instead')
const SaveStatisticResponse$json = const {
  '1': 'SaveStatisticResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `SaveStatisticResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List saveStatisticResponseDescriptor = $convert.base64Decode('ChVTYXZlU3RhdGlzdGljUmVzcG9uc2USFgoGc3RhdHVzGAEgASgJUgZzdGF0dXM=');
@$core.Deprecated('Use getStatisticRequestDescriptor instead')
const GetStatisticRequest$json = const {
  '1': 'GetStatisticRequest',
  '2': const [
    const {'1': 'uuid', '3': 1, '4': 1, '5': 9, '10': 'uuid'},
  ],
};

/// Descriptor for `GetStatisticRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getStatisticRequestDescriptor = $convert.base64Decode('ChNHZXRTdGF0aXN0aWNSZXF1ZXN0EhIKBHV1aWQYASABKAlSBHV1aWQ=');
@$core.Deprecated('Use getStatisticResponseDescriptor instead')
const GetStatisticResponse$json = const {
  '1': 'GetStatisticResponse',
  '2': const [
    const {'1': 'node_auth_data', '3': 1, '4': 1, '5': 11, '6': '.Common.NodeAuthData', '10': 'nodeAuthData'},
    const {'1': 'statistic', '3': 2, '4': 1, '5': 11, '6': '.cardano.StatisticResponse', '10': 'statistic'},
  ],
};

/// Descriptor for `GetStatisticResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getStatisticResponseDescriptor = $convert.base64Decode('ChRHZXRTdGF0aXN0aWNSZXNwb25zZRI6Cg5ub2RlX2F1dGhfZGF0YRgBIAEoCzIULkNvbW1vbi5Ob2RlQXV0aERhdGFSDG5vZGVBdXRoRGF0YRI4CglzdGF0aXN0aWMYAiABKAsyGi5jYXJkYW5vLlN0YXRpc3RpY1Jlc3BvbnNlUglzdGF0aXN0aWM=');
@$core.Deprecated('Use statisticRequestDescriptor instead')
const StatisticRequest$json = const {
  '1': 'StatisticRequest',
  '2': const [
    const {'1': 'node_basic_data', '3': 1, '4': 1, '5': 11, '6': '.Common.NodeBasicData', '10': 'nodeBasicData'},
    const {'1': 'server_basic_data', '3': 2, '4': 1, '5': 11, '6': '.Common.ServerBasicData', '10': 'serverBasicData'},
    const {'1': 'updates', '3': 3, '4': 1, '5': 11, '6': '.Common.Updates', '10': 'updates'},
    const {'1': 'security', '3': 4, '4': 1, '5': 11, '6': '.Common.Security', '10': 'security'},
    const {'1': 'epoch', '3': 5, '4': 1, '5': 11, '6': '.cardano.Epoch', '10': 'epoch'},
    const {'1': 'kes_data', '3': 6, '4': 1, '5': 11, '6': '.cardano.KESData', '10': 'kesData'},
    const {'1': 'blocks', '3': 7, '4': 1, '5': 11, '6': '.cardano.Blocks', '10': 'blocks'},
    const {'1': 'stake_info', '3': 8, '4': 1, '5': 11, '6': '.cardano.StakeInfo', '10': 'stakeInfo'},
    const {'1': 'online', '3': 9, '4': 1, '5': 11, '6': '.Common.Online', '10': 'online'},
    const {'1': 'memory_state', '3': 10, '4': 1, '5': 11, '6': '.Common.MemoryState', '10': 'memoryState'},
    const {'1': 'cpu_state', '3': 11, '4': 1, '5': 11, '6': '.Common.CPUState', '10': 'cpuState'},
    const {'1': 'node_state', '3': 12, '4': 1, '5': 11, '6': '.cardano.NodeState', '10': 'nodeState'},
    const {'1': 'node_performance', '3': 13, '4': 1, '5': 11, '6': '.cardano.NodePerformance', '10': 'nodePerformance'},
  ],
};

/// Descriptor for `StatisticRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List statisticRequestDescriptor = $convert.base64Decode('ChBTdGF0aXN0aWNSZXF1ZXN0Ej0KD25vZGVfYmFzaWNfZGF0YRgBIAEoCzIVLkNvbW1vbi5Ob2RlQmFzaWNEYXRhUg1ub2RlQmFzaWNEYXRhEkMKEXNlcnZlcl9iYXNpY19kYXRhGAIgASgLMhcuQ29tbW9uLlNlcnZlckJhc2ljRGF0YVIPc2VydmVyQmFzaWNEYXRhEikKB3VwZGF0ZXMYAyABKAsyDy5Db21tb24uVXBkYXRlc1IHdXBkYXRlcxIsCghzZWN1cml0eRgEIAEoCzIQLkNvbW1vbi5TZWN1cml0eVIIc2VjdXJpdHkSJAoFZXBvY2gYBSABKAsyDi5jYXJkYW5vLkVwb2NoUgVlcG9jaBIrCghrZXNfZGF0YRgGIAEoCzIQLmNhcmRhbm8uS0VTRGF0YVIHa2VzRGF0YRInCgZibG9ja3MYByABKAsyDy5jYXJkYW5vLkJsb2Nrc1IGYmxvY2tzEjEKCnN0YWtlX2luZm8YCCABKAsyEi5jYXJkYW5vLlN0YWtlSW5mb1IJc3Rha2VJbmZvEiYKBm9ubGluZRgJIAEoCzIOLkNvbW1vbi5PbmxpbmVSBm9ubGluZRI2CgxtZW1vcnlfc3RhdGUYCiABKAsyEy5Db21tb24uTWVtb3J5U3RhdGVSC21lbW9yeVN0YXRlEi0KCWNwdV9zdGF0ZRgLIAEoCzIQLkNvbW1vbi5DUFVTdGF0ZVIIY3B1U3RhdGUSMQoKbm9kZV9zdGF0ZRgMIAEoCzISLmNhcmRhbm8uTm9kZVN0YXRlUglub2RlU3RhdGUSQwoQbm9kZV9wZXJmb3JtYW5jZRgNIAEoCzIYLmNhcmRhbm8uTm9kZVBlcmZvcm1hbmNlUg9ub2RlUGVyZm9ybWFuY2U=');
@$core.Deprecated('Use statisticResponseDescriptor instead')
const StatisticResponse$json = const {
  '1': 'StatisticResponse',
  '2': const [
    const {'1': 'node_basic_data', '3': 1, '4': 1, '5': 11, '6': '.Common.NodeBasicDataResponse', '10': 'nodeBasicData'},
    const {'1': 'server_basic_data', '3': 2, '4': 1, '5': 11, '6': '.Common.ServerBasicDataResponse', '10': 'serverBasicData'},
    const {'1': 'updates', '3': 3, '4': 1, '5': 11, '6': '.Common.UpdatesResponse', '10': 'updates'},
    const {'1': 'security', '3': 4, '4': 1, '5': 11, '6': '.Common.SecurityResponse', '10': 'security'},
    const {'1': 'epoch', '3': 5, '4': 1, '5': 11, '6': '.cardano.EpochResponse', '10': 'epoch'},
    const {'1': 'kes_data', '3': 6, '4': 1, '5': 11, '6': '.cardano.KESDataResponse', '10': 'kesData'},
    const {'1': 'blocks', '3': 7, '4': 1, '5': 11, '6': '.cardano.BlocksResponse', '10': 'blocks'},
    const {'1': 'stake_info', '3': 8, '4': 1, '5': 11, '6': '.cardano.StakeInfoResponse', '10': 'stakeInfo'},
    const {'1': 'online', '3': 9, '4': 1, '5': 11, '6': '.Common.OnlineResponse', '10': 'online'},
    const {'1': 'memory_state', '3': 10, '4': 1, '5': 11, '6': '.Common.MemoryStateResponse', '10': 'memoryState'},
    const {'1': 'cpu_state', '3': 11, '4': 1, '5': 11, '6': '.Common.CPUStateResponse', '10': 'cpuState'},
    const {'1': 'node_state', '3': 12, '4': 1, '5': 11, '6': '.cardano.NodeStateResponse', '10': 'nodeState'},
    const {'1': 'node_performance', '3': 13, '4': 1, '5': 11, '6': '.cardano.NodePerformanceResponse', '10': 'nodePerformance'},
  ],
};

/// Descriptor for `StatisticResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List statisticResponseDescriptor = $convert.base64Decode('ChFTdGF0aXN0aWNSZXNwb25zZRJFCg9ub2RlX2Jhc2ljX2RhdGEYASABKAsyHS5Db21tb24uTm9kZUJhc2ljRGF0YVJlc3BvbnNlUg1ub2RlQmFzaWNEYXRhEksKEXNlcnZlcl9iYXNpY19kYXRhGAIgASgLMh8uQ29tbW9uLlNlcnZlckJhc2ljRGF0YVJlc3BvbnNlUg9zZXJ2ZXJCYXNpY0RhdGESMQoHdXBkYXRlcxgDIAEoCzIXLkNvbW1vbi5VcGRhdGVzUmVzcG9uc2VSB3VwZGF0ZXMSNAoIc2VjdXJpdHkYBCABKAsyGC5Db21tb24uU2VjdXJpdHlSZXNwb25zZVIIc2VjdXJpdHkSLAoFZXBvY2gYBSABKAsyFi5jYXJkYW5vLkVwb2NoUmVzcG9uc2VSBWVwb2NoEjMKCGtlc19kYXRhGAYgASgLMhguY2FyZGFuby5LRVNEYXRhUmVzcG9uc2VSB2tlc0RhdGESLwoGYmxvY2tzGAcgASgLMhcuY2FyZGFuby5CbG9ja3NSZXNwb25zZVIGYmxvY2tzEjkKCnN0YWtlX2luZm8YCCABKAsyGi5jYXJkYW5vLlN0YWtlSW5mb1Jlc3BvbnNlUglzdGFrZUluZm8SLgoGb25saW5lGAkgASgLMhYuQ29tbW9uLk9ubGluZVJlc3BvbnNlUgZvbmxpbmUSPgoMbWVtb3J5X3N0YXRlGAogASgLMhsuQ29tbW9uLk1lbW9yeVN0YXRlUmVzcG9uc2VSC21lbW9yeVN0YXRlEjUKCWNwdV9zdGF0ZRgLIAEoCzIYLkNvbW1vbi5DUFVTdGF0ZVJlc3BvbnNlUghjcHVTdGF0ZRI5Cgpub2RlX3N0YXRlGAwgASgLMhouY2FyZGFuby5Ob2RlU3RhdGVSZXNwb25zZVIJbm9kZVN0YXRlEksKEG5vZGVfcGVyZm9ybWFuY2UYDSABKAsyIC5jYXJkYW5vLk5vZGVQZXJmb3JtYW5jZVJlc3BvbnNlUg9ub2RlUGVyZm9ybWFuY2U=');
@$core.Deprecated('Use epochResponseDescriptor instead')
const EpochResponse$json = const {
  '1': 'EpochResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 1, '5': 11, '6': '.cardano.Epoch', '10': 'data'},
    const {'1': 'status', '3': 2, '4': 1, '5': 11, '6': '.Common.Status', '10': 'status'},
  ],
};

/// Descriptor for `EpochResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List epochResponseDescriptor = $convert.base64Decode('Cg1FcG9jaFJlc3BvbnNlEiIKBGRhdGEYASABKAsyDi5jYXJkYW5vLkVwb2NoUgRkYXRhEiYKBnN0YXR1cxgCIAEoCzIOLkNvbW1vbi5TdGF0dXNSBnN0YXR1cw==');
@$core.Deprecated('Use kESDataResponseDescriptor instead')
const KESDataResponse$json = const {
  '1': 'KESDataResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 1, '5': 11, '6': '.cardano.KESData', '10': 'data'},
    const {'1': 'status', '3': 2, '4': 1, '5': 11, '6': '.Common.Status', '10': 'status'},
  ],
};

/// Descriptor for `KESDataResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List kESDataResponseDescriptor = $convert.base64Decode('Cg9LRVNEYXRhUmVzcG9uc2USJAoEZGF0YRgBIAEoCzIQLmNhcmRhbm8uS0VTRGF0YVIEZGF0YRImCgZzdGF0dXMYAiABKAsyDi5Db21tb24uU3RhdHVzUgZzdGF0dXM=');
@$core.Deprecated('Use blocksResponseDescriptor instead')
const BlocksResponse$json = const {
  '1': 'BlocksResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 1, '5': 11, '6': '.cardano.Blocks', '10': 'data'},
    const {'1': 'status', '3': 2, '4': 1, '5': 11, '6': '.Common.Status', '10': 'status'},
  ],
};

/// Descriptor for `BlocksResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List blocksResponseDescriptor = $convert.base64Decode('Cg5CbG9ja3NSZXNwb25zZRIjCgRkYXRhGAEgASgLMg8uY2FyZGFuby5CbG9ja3NSBGRhdGESJgoGc3RhdHVzGAIgASgLMg4uQ29tbW9uLlN0YXR1c1IGc3RhdHVz');
@$core.Deprecated('Use stakeInfoResponseDescriptor instead')
const StakeInfoResponse$json = const {
  '1': 'StakeInfoResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 1, '5': 11, '6': '.cardano.StakeInfo', '10': 'data'},
    const {'1': 'status', '3': 2, '4': 1, '5': 11, '6': '.Common.Status', '10': 'status'},
  ],
};

/// Descriptor for `StakeInfoResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List stakeInfoResponseDescriptor = $convert.base64Decode('ChFTdGFrZUluZm9SZXNwb25zZRImCgRkYXRhGAEgASgLMhIuY2FyZGFuby5TdGFrZUluZm9SBGRhdGESJgoGc3RhdHVzGAIgASgLMg4uQ29tbW9uLlN0YXR1c1IGc3RhdHVz');
@$core.Deprecated('Use nodeStateResponseDescriptor instead')
const NodeStateResponse$json = const {
  '1': 'NodeStateResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 1, '5': 11, '6': '.cardano.NodeState', '10': 'data'},
    const {'1': 'status', '3': 2, '4': 1, '5': 11, '6': '.Common.Status', '10': 'status'},
  ],
};

/// Descriptor for `NodeStateResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nodeStateResponseDescriptor = $convert.base64Decode('ChFOb2RlU3RhdGVSZXNwb25zZRImCgRkYXRhGAEgASgLMhIuY2FyZGFuby5Ob2RlU3RhdGVSBGRhdGESJgoGc3RhdHVzGAIgASgLMg4uQ29tbW9uLlN0YXR1c1IGc3RhdHVz');
@$core.Deprecated('Use nodePerformanceResponseDescriptor instead')
const NodePerformanceResponse$json = const {
  '1': 'NodePerformanceResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 1, '5': 11, '6': '.cardano.NodePerformance', '10': 'data'},
    const {'1': 'status', '3': 2, '4': 1, '5': 11, '6': '.Common.Status', '10': 'status'},
  ],
};

/// Descriptor for `NodePerformanceResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nodePerformanceResponseDescriptor = $convert.base64Decode('ChdOb2RlUGVyZm9ybWFuY2VSZXNwb25zZRIsCgRkYXRhGAEgASgLMhguY2FyZGFuby5Ob2RlUGVyZm9ybWFuY2VSBGRhdGESJgoGc3RhdHVzGAIgASgLMg4uQ29tbW9uLlN0YXR1c1IGc3RhdHVz');
@$core.Deprecated('Use epochDescriptor instead')
const Epoch$json = const {
  '1': 'Epoch',
  '2': const [
    const {'1': 'epoch_number', '3': 1, '4': 1, '5': 3, '10': 'epochNumber'},
  ],
};

/// Descriptor for `Epoch`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List epochDescriptor = $convert.base64Decode('CgVFcG9jaBIhCgxlcG9jaF9udW1iZXIYASABKANSC2Vwb2NoTnVtYmVy');
@$core.Deprecated('Use kESDataDescriptor instead')
const KESData$json = const {
  '1': 'KESData',
  '2': const [
    const {'1': 'kes_current', '3': 1, '4': 1, '5': 3, '10': 'kesCurrent'},
    const {'1': 'kes_remaining', '3': 2, '4': 1, '5': 3, '10': 'kesRemaining'},
    const {'1': 'kes_exp_date', '3': 3, '4': 1, '5': 9, '10': 'kesExpDate'},
  ],
};

/// Descriptor for `KESData`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List kESDataDescriptor = $convert.base64Decode('CgdLRVNEYXRhEh8KC2tlc19jdXJyZW50GAEgASgDUgprZXNDdXJyZW50EiMKDWtlc19yZW1haW5pbmcYAiABKANSDGtlc1JlbWFpbmluZxIgCgxrZXNfZXhwX2RhdGUYAyABKAlSCmtlc0V4cERhdGU=');
@$core.Deprecated('Use blocksDescriptor instead')
const Blocks$json = const {
  '1': 'Blocks',
  '2': const [
    const {'1': 'block_leader', '3': 1, '4': 1, '5': 3, '10': 'blockLeader'},
    const {'1': 'block_adopted', '3': 2, '4': 1, '5': 3, '10': 'blockAdopted'},
    const {'1': 'block_invalid', '3': 3, '4': 1, '5': 3, '10': 'blockInvalid'},
  ],
};

/// Descriptor for `Blocks`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List blocksDescriptor = $convert.base64Decode('CgZCbG9ja3MSIQoMYmxvY2tfbGVhZGVyGAEgASgDUgtibG9ja0xlYWRlchIjCg1ibG9ja19hZG9wdGVkGAIgASgDUgxibG9ja0Fkb3B0ZWQSIwoNYmxvY2tfaW52YWxpZBgDIAEoA1IMYmxvY2tJbnZhbGlk');
@$core.Deprecated('Use stakeInfoDescriptor instead')
const StakeInfo$json = const {
  '1': 'StakeInfo',
  '2': const [
    const {'1': 'live_stake', '3': 1, '4': 1, '5': 3, '10': 'liveStake'},
    const {'1': 'active_stake', '3': 2, '4': 1, '5': 3, '10': 'activeStake'},
    const {'1': 'pledge', '3': 3, '4': 1, '5': 3, '10': 'pledge'},
  ],
};

/// Descriptor for `StakeInfo`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List stakeInfoDescriptor = $convert.base64Decode('CglTdGFrZUluZm8SHQoKbGl2ZV9zdGFrZRgBIAEoA1IJbGl2ZVN0YWtlEiEKDGFjdGl2ZV9zdGFrZRgCIAEoA1ILYWN0aXZlU3Rha2USFgoGcGxlZGdlGAMgASgDUgZwbGVkZ2U=');
@$core.Deprecated('Use nodeStateDescriptor instead')
const NodeState$json = const {
  '1': 'NodeState',
  '2': const [
    const {'1': 'tip_diff', '3': 1, '4': 1, '5': 3, '10': 'tipDiff'},
    const {'1': 'density', '3': 2, '4': 1, '5': 2, '10': 'density'},
  ],
};

/// Descriptor for `NodeState`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nodeStateDescriptor = $convert.base64Decode('CglOb2RlU3RhdGUSGQoIdGlwX2RpZmYYASABKANSB3RpcERpZmYSGAoHZGVuc2l0eRgCIAEoAlIHZGVuc2l0eQ==');
@$core.Deprecated('Use nodePerformanceDescriptor instead')
const NodePerformance$json = const {
  '1': 'NodePerformance',
  '2': const [
    const {'1': 'processed_tx', '3': 1, '4': 1, '5': 3, '10': 'processedTx'},
    const {'1': 'peers_in', '3': 2, '4': 1, '5': 3, '10': 'peersIn'},
    const {'1': 'peers_out', '3': 3, '4': 1, '5': 3, '10': 'peersOut'},
  ],
};

/// Descriptor for `NodePerformance`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nodePerformanceDescriptor = $convert.base64Decode('Cg9Ob2RlUGVyZm9ybWFuY2USIQoMcHJvY2Vzc2VkX3R4GAEgASgDUgtwcm9jZXNzZWRUeBIZCghwZWVyc19pbhgCIAEoA1IHcGVlcnNJbhIbCglwZWVyc19vdXQYAyABKANSCHBlZXJzT3V0');

///
//  Generated code. Do not modify.
//  source: common/common.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'common.pb.dart' as $0;
export 'common.pb.dart';

class ControllerClient extends $grpc.Client {
  static final _$getNodeList =
      $grpc.ClientMethod<$0.GetNodeListRequest, $0.GetNodeListResponse>(
          '/Common.Controller/GetNodeList',
          ($0.GetNodeListRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.GetNodeListResponse.fromBuffer(value));

  ControllerClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.GetNodeListResponse> getNodeList(
      $0.GetNodeListRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getNodeList, request, options: options);
  }
}

abstract class ControllerServiceBase extends $grpc.Service {
  $core.String get $name => 'Common.Controller';

  ControllerServiceBase() {
    $addMethod(
        $grpc.ServiceMethod<$0.GetNodeListRequest, $0.GetNodeListResponse>(
            'GetNodeList',
            getNodeList_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.GetNodeListRequest.fromBuffer(value),
            ($0.GetNodeListResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.GetNodeListResponse> getNodeList_Pre($grpc.ServiceCall call,
      $async.Future<$0.GetNodeListRequest> request) async {
    return getNodeList(call, await request);
  }

  $async.Future<$0.GetNodeListResponse> getNodeList(
      $grpc.ServiceCall call, $0.GetNodeListRequest request);
}
